# SiPR – ćwiczenie 3 – Kinematyka i sterowanie pozycyjne manipulatora

## Przygotowanie środowiska pracy

Instrukcja przygotowania środowiska pracy zakłada, że wykonane zostało ćwiczenie [Przygotowanie środowiska ROS](https://bitbucket.org/rrgwut/sipr_lab0).

### Instalacja brakujących pakietów

Przed instalacją nowych pakietów z repozytorium Ubuntu należy zainstalować ewentualne aktualizacje. W konsoli `yakuake` (`F12`) wykonujemy polecenia:

```
sudo apt update
```

```
sudo apt upgrade
```

Instalacja brakujących pakietów ROS-a z repozytorium Ubuntu (Ubuntu 20.04, ROS noetic) wymaga uruchomienia w konsoli następujących poleceń (wciśnij `F12`, aby otworzyć terminal `yakuake`):

```
sudo apt-get install -y ros-noetic-moveit ros-noetic-industrial-robot-client
```

```
cd ~/sipr_ws/src
```

Ściągniecie pakietów z modelami robotów Fanuc:

```
git clone -b kinetic https://github.com/ros-industrial/fanuc.git
```

Wyłączanie z budowania pakietów z modelami robotów innych niż Fanuc M10iA poprzez stworzenie pustych plików `CATKIN_IGNORE`:

**Uwaga: w poniższym poleceniu wszystkie znaki i spacje są ważne!**

```
find fanuc -mindepth 1 -maxdepth 1 -type d \( ! -iname "*m10ia*" ! -iname ".*" ! -iname "fanuc_resources" \) -exec touch {}/CATKIN_IGNORE \;
```

### Przygotowanie przestrzeni roboczej

Wchodzimy do podkatalogu `src` przestrzeni roboczej `sipr_ws`:

```
cd ~/sipr_ws/src
```

Ściągamy kod źródłowy pakietu `sipr_lab3` przygotowanego jako szablon do dalszego wykonania instrukcji:

```
git clone https://bitbucket.org/rrgwut/sipr_lab3
```

Wywołujemy budowanie przestrzeni roboczej `sipr_ws`:

```
cd ~/sipr_ws
```

```
catkin_make -DCMAKE_BUILD_TYPE=Debug
```

### Uruchomienie środowiska VS Code

Uruchamiamy VS Code w przestrzeni roboczej `sipr_ws`:

```
code ~/sipr_ws
```

W VS Code zobaczymy zawartość całej przestrzeni roboczej `sipr_ws` w tym otwarte pliki z poprzednich ćwiczeń, które można zamknąć. W drzewie katalogów można zwinąć katalogi poprzednich ćwiczeń i rozwinąć katalog `sipr_lab3`, w którym znajdziemy tą instrukcję w pliku [`README.md`](./README.md).

> **Uwaga:** czytanie instrukcji należy kontynuować z poziomu VS Code, który poprawnie wyświetla wzory (w Bitbucket zamiast wzorów pojawia się kod źródłowy języka LateX).
>
> W drzewie katalogów należy:
>
>* odnaleźć plik [`sipr_ws/src/sipr_lab3/README.md`](./README.md),
>* wcisnąć skrót `Ctrl+Shift+P`,
>* rozpocząć pisanie polecenia `markdown preview`
>* i wybrać opcję `Markdown: Open Locked Preview to the Side`.
>
> Edytor z kodem pliku [README.md](./README.md) można zamknąć.

#### Konfiguracji debuggera w VS Code

W VS Code należy wcisnąć skrót przycisk `Ctrl+Shift+D`, który przeniesienie nas z widoku drzewa katalogów do widoku `Uruchamiania`.

1. Jeżeli debugger **nie był jeszcze konfigurowany** należy wybrać opcję `create a launch.json file`, a dalej opcję `C++ (GDB/LLDB)`.
2. Jeżeli debugger **był już konfigurowany** należy wcisnąć przycisk `Open launch.json` (ten z trybikiem), a następnie w otwartym pliku [`sipr_ws/.vscode/launch.json`](../../.vscode/launch.json) kliknąć przycisk `Add configuration...`, który pojawia się w dolnym prawym rogu okna edycji, a następnie wybieramy `C/C++: (gdb) Launch`.

W pliku [`sipr_ws/.vscode/launch.json`](../../.vscode/launch.json) zmieniamy następujące linie:

```
...
            "name": "(gdb) sipr_lab3_node",
...
            "program": "${workspaceFolder}/devel/lib/sipr_lab3/sipr_lab3_node",
...
            "cwd": "${workspaceFolder}",
...
```

Na koniec możemy sprawdzić czy węzeł `sipr_lab3_node` – który jest wstępnie zaprogramowany – działa. W tym celu w widoku uruchamiania (`Ctrl+Shift+D`) na liście rozwijanej wybieramy opcję `(gdb) sipr_lab3_node` i wciskamy `F5` lub przycisk z zieloną strzałką.

W tym momencie nie jest uruchomiony `ROS master`, więc węzeł `sipr_lab3_node` w terminalu (u dołu okna VS Code) pokaże błąd.

> Uruchomiony węzeł można zamknąć skrótem klawiszowym `Shift+F5` lub wciskając czerwony kwadrat z panelu debugowania.

## Wykonanie ćwiczenia

To ćwiczenie poświęcone jest implementacji prostego kontrolera ruchu manipulatora realizującego sterowanie pozycyjne. W ramach przygotowanego oprogramowania dostępny jest prosty symulator wizualizacja oraz wstępna implementacja kontrolera ruchu. Końcowy efekt ćwiczenia został pokazany na animacji poniżej.

![image](doc/figures/robot_program_run.gif)

Wizualizacja w `rviz` przedstawia robota i wybrane pozycje jego narzędzia. Dużą żółtą strzałką oznaczono aktualną pozycję narzędzia robota, mniejsze żółte strzałki pokazują przebytą ścieżkę. Strzałkami czerwonymi oznaczono punkty do osiągnięcia  zadane w programie sterującym robota. Strzałka zielona reprezentuje najbliższy cel do zrealizowania. Domyślnie zaimplementowany program robota przewiduje ruch z pozycji początkowej do pierwszej pozycji korzystając z interpolacji w przestrzeni złączy. Następnie realizowany jest ruch z interpolacją liniową w przestrzeni kartezjańskiej 3D (przestrzeni zadaniowej). Na koniec robot wraca do pozycji początkowej korzystając z interpolacji w przestrzeni złączy.

**Do wykonania są następujące zadania:**

1. Zapoznanie się z możliwościami oprogramowania:

    * `rviz`,
    * `joint_state_publisher_gui`.

2. Zapoznanie się z kodem źródłowym kontrolera ruchu.
3. Obliczenie błędu położenia dla każdej z osi robota.
4. Sterowanie do pozycji zadanej w przestrzeni złączy - dobór nastaw regulatora PID.
5. Obliczenie punktów pośrednich dla ruchu po linii prostej (interpolacja liniowa ruchu w przestrzeni zadaniowej).
6. Testy i analiza otrzymanego rozwiązania.

### 1. Zapoznanie się z możliwościami oprogramowania

W ramach pakietu `sipr_lab3` przygotowany został węzeł ROS-a o nazwie `sipr_lab3_node` oraz symulacja robota Fanuc M10iA.

Najpierw uruchomimy symulację. W terminalu `yakuake` (`F12`) włączamy skrypt `roslaunch`'a uruchamiający wizualizację w `rviz`'ie.

```
roslaunch sipr_lab3 setup.launch
```

Położenie każdej z osi robota można zmienić ręcznie korzystając z programu `joint_state_publisher_gui`.

Następnie z poziomu VS Code uruchamiamy węzeł `sipr_lab3_node` za pomocą klawisza `F5`. Na ekranie pojawią się strzałki reprezentujące aktualną pozycję robota oraz punkty programu sterującego, ale robot nie będzie się jeszcze poruszał - to wymaga odpowiednich modyfikacji kodu.

![image](doc/figures/rviz_p1.png)

### 2. Kod źródłowy pakietu `sipr_lab3`

#### 2.1. Węzeł `sipr_lab3_node`

Węzeł `sipr_lab3_node` zaimplementowany jest w pliku [`src/sipr_lab3_node.cpp`](src/sipr_lab3_node.cpp). Znajduje się tam główna pętla programu, która w każdym obrocie aktualizuje stan prostego symulatora zaimplementowanego w klasie `Simulator` (definicja klasy w pliku [include/sipr_lab3/Simulator.h](include/sipr_lab3/Simulator.h)).

Symulator, w każdym kroku, aktualizuje pozycję złączy robota na podstawie prędkości zadanej przez kontroler (symulator subskrybuje wiadomość typu `control_msgs::JointJog` na kanale `/sipr_lab3_node/joint_jog`), a następnie publikuje aktualne położenie złącz robota na kanale `/sipr_lab3_node/joint_states` (wiadomość typu `sensor_msgs::JointState`).

#### 2.2. Kontroler ruchu `JointController`

Główne zadania kontrolera robota realizowane są przez obiekt `controller` klasy `JointController` zaimplementowanej w plikach:

* [`include/sipr_lab3/JointController.h`](include/sipr_lab3/JointController.h)
* [`src/sipr_lab3/JointController.cpp`](src/sipr_lab3/JointController.cpp).

#### 2.2.1. Funkcje i metody pomocnicze

Klasa `JointController` zawiera kilka funkcji przydatnych w dalszych częściach ćwiczenia:

* `calcEuclideanDistance` – funkcja obliczająca odległość w metrach między dwiema pozycjami,
* `calcAngularDistance` – funkcja obliczająca odległość w radianach między dwiema orientacjami robota,
* `JointController::isometry3dToPoseStamped` – przelicza pozycję robota z obiektu klasy `Eigen::Isometry3d` na wiadomość ROS-a typu `geometry_msgs::PoseStamped`,
* `JointController::currentToolPosition` – metoda zwracająca aktualną pozycję robota w przestrzeni kartezjańskiej w postaci obiektu typu `Eigen::Isometry3d`,
* `JointController::currentJointPositions` – metoda zwracająca aktualną pozycję robota w przestrzeni złączy jako wektor sześciu liczb rzeczywistych `std::vector< double >` reprezentujących kąty w radianach,
* `JointController::getRandomPosition()` – metoda zwracająca losową pozycję robota  w przestrzeni kartezjańskiej w postaci obiektu typu `Eigen::Isometry3d`.

#### 2.2.2. Metody obliczające kinematykę robota

W programie sterującym robota kluczową rolę odgrywają funkcje obliczające kinematykę prostą i odwrotną. Tutaj skorzystamy z gotowych obliczeń dostępnych w ramach biblioteki `MoveIt!`.

Implementacja metod `JointController::forwardKinematics` i `JointController::inverseKinematics` ogranicza się do wywołania odpowiednich funkcji z `MoveIt!` oraz obsługi sytuacji wyjątkowych.

Obliczenie kinematyki prostej ogranicza się do trzech instrukcji:

```c++
Eigen::Isometry3d JointController::forwardKinematics(std::vector< double > joint_positions)
{
    robot_state::RobotState robot_state(kinematic_model_);
    robot_state.setJointGroupPositions(joint_model_group_, joint_positions);
    return robot_state.getGlobalLinkTransform("tool0");
}
```

Obliczenie kinematyki odwrotnej jest realizowane za pomocą metody iteracyjnej, a więc w pewnym ograniczonym przedziale czasu `timeout = 0.1` podejmowane są próby wyznaczenia pozycji złączy robota, dla których robot osiągnie pozycję narzędzia zadaną w przestrzeni kartezjańskiej. Może się jednak zdarzyć, że rozwiązenie nie zostanie odnalezione, wtedy zostanie wypisany komunikat o błędzie `ROS_ERROR("Did not find IK solution")` i program zakończy działanie `ros::shutdown()`.

```c++
std::vector<double> JointController::inverseKinematics(Eigen::Isometry3d end_effector_state)
{
    std::vector<double> joint_values;

    robot_state::RobotState robot_state(kinematic_model_);

    double timeout = 0.1;
    bool found_ik = robot_state.setFromIK(joint_model_group_, end_effector_state, timeout);

    if (found_ik && robot_state.satisfiesBounds())
    {
        robot_state.copyJointGroupPositions(joint_model_group_, joint_values);
    }
    else
    {
        ROS_ERROR("Did not find IK solution");
        ros::shutdown();
    }

    return joint_values; //Uwaga:joint_values będzie puste jeżeli nie uda się 
                         //znaleźć rozwiązania kinematyki odwrotnej
}
```

#### 2.2.4. Implementacja programu robota

Zaimplementowany tutaj kontroler robota jest w stanie realizować proste instrukcje dojazdu od punktu do punktu typowe dla robotów przemysłowych. Możliwe są tutaj ruchy po najkrótszej ścieżce w przestrzeni złączy (`Interpolation::Joint`) oraz ruch po najkrótszej ścieżce w przestrzeni kartezjańskiej, czyli po linii prostej (`Interpolation::Linear`).

Program robota zdefiniowany jest w metodzie `JointController::defineRobotProgram`. Program zakłada przejazd między trzema punktami, z czego ostatni punkt jest pozycją początkową robota. Przejazd między punktami `p1` i `p2` odbywa się po linii prostej, a pozostałe ruchy są realizowane jako przejazd po najkrótszej ścieżce w przestrzeni złączy.

```c++
void JointController::defineRobotProgram()
{
    robot_program_.clear();

    RobotProgramInstruction p1;
    p1.interpolation = Interpolation::Joint; // Ustawienie interpolacji w przestrzeni węzłów
    // p1.position = getRandomPosition(); // Nadanie losowej pozycji, można zastąpić własnymi danymi wg wzoru poniżej
    // Nadanie wartości macierzy przekształcenia jednorodnego:
    p1.position.setIdentity();
    p1.position.matrix().block<3,1>(0,3) = Eigen::Vector3d(1.2, -0.4, 0.6);
    Eigen::Matrix3d p1_rotation(  Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitX()));
    p1.position.matrix().block<3,3>(0,0) = p1_rotation;
    robot_program_.push_back(p1); // Dodanie instrukcji do programu robota


    RobotProgramInstruction p2;
    p2.interpolation = Interpolation::Linear; // Ustawienie interpolacji w przestrzeni kartezjańskiej
    // p2.position = getRandomPosition(); // Nadanie losowej pozycji, można zastąpić własnymi danymi wg wzoru poniżej
    // Nadanie wartości macierzy przekształcenia jednorodnego:
    p2.position.setIdentity();
    p2.position.matrix().block<3,1>(0,3) = Eigen::Vector3d(1.2, 0.4, 0.6);
    Eigen::Matrix3d p2_rotation(  Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitZ())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitX()));
    p2.position.matrix().block<3,3>(0,0) = p2_rotation;
    robot_program_.push_back(p2); // Dodanie instrukcji do programu robota


    RobotProgramInstruction p3;
    p3.interpolation = Interpolation::Joint; // Ustawienie interpolacji w przestrzeni węzłów
    p3.position = forwardKinematics(std::vector<double>(6, 0.0)); // Ustawienie pozycji zerowej
    robot_program_.push_back(p3); // Dodanie instrukcji do programu robota
}
```

W programie zadane są konkretne wartości pozycji, ale możliwe jest również wybranie losowych punktów poprzez odkomentowanie instrukcji `getRandomPosition()`.

#### 2.2.5. Metoda `JointController::jointStatesCallback`

Obiekt klasy `JointController` reaguje na wiadomości na kanale `/sipr_lab3_node/joint_states` (wiadomość typu `sensor_msgs::JointState`) i tylko wtedy wykonuje obliczenia. Wiadomość ta zawiera aktualne położenie kątowe każdej z osi robota.

Przyjście nowej wiadomości na kanale `/sipr_lab3_node/joint_states` powoduje uruchomienie metody `JointController::jointStatesCallback` ([`src/sipr_lab3/JointController.cpp`, linia 385](src/sipr_lab3/JointController.cpp#L385)).

Pierwsze instrukcje w tej metodzie to aktualizacja wewnętrznej reprezentacji stanu robota (zmienna `current_robot_state_`):

```c++
    // Aktualizacja stanu robota
    current_robot_state_->setVariableValues(joint_states_msg);

    // Publikowanie aktualnej pozycji narzędzia (żółta strzałka w rviz)
    const Eigen::Isometry3d& current_tool_position = currentToolPosition();
    pub_current_pose_.publish(isometry3dToPoseStamped(current_tool_position));
```

Reszta metody `JointController::jointStatesCallback` zawiera obsługę kolejnej instrukcji programu sterującego robota. W zależności od tego jakiego typu ruch ma być wykonany, sterowanie w przestrzeni złączy, czy ruch liniowy, wyznaczany jest najbliższy cel (zielona strzałka w `rviz`):

```c++
        Eigen::Isometry3d target;

        if(instruction.interpolation == Interpolation::Joint)
        {
            target = instruction.position;
        }
        else if(instruction.interpolation == Interpolation::Linear)
        {
            if(!linear_motion_initialized)
            {
                initializeLinearMotion(current_tool_position, instruction.position);
            }

            target = getInterpolated3dPose(ros::Time::now());
        }

        pub_current_target_.publish(isometry3dToPoseStamped(target));
```

Następnie, dla aktualnej pozycji robota i aktualnego celu obliczane są błędy pozycji osi robota:

```c++
        ///////////// Obliczenie kierunków przemieszczania konkretnych złączy
        std::vector<double> joint_position_errors;

        ///////////// Metoda 1
        std::vector<double> target_joint_values = inverseKinematics(target);
        if(!target_joint_values.empty())
        {
            joint_position_errors = jointPositionError(
                currentJointPositions(), target_joint_values);
        }
```

Na koniec obliczane są nowe sygnały sterujące – prędkości obrotowe każdej z osi (wiadomość typu `control_msgs::JointJog` na kanale `/sipr_lab3_node/joint_jog`):

[`src/sipr_lab3/JointController.cpp`, linia 385:](src/sipr_lab3/JointController.cpp#L385)
```c++
    std::vector<double> joint_velocities = jointPositionControl(joint_position_errors);
    publishJointJog(joint_velocities);
```

Należy tutaj zwrócić uwagę na fakt, że nawet w przypadku ruchu po linii prostej, sterowanie polega na obliczeniu prędkości obrotowej dla każdej z osi. Ruch po linii prostej realizowany jest w ten sposób, że w każdej chwili obliczany jest lokalny cel na tej prostej, ale samo sterowanie odbywa się już w przestrzeni złączy.

W przedstawionym tu kontrolerze, wszystkie pozycje głównego programu robota oraz chwilowe cele wyliczone w interpolacji liniowej zadane są przestrzeni kartezjańskiej (zadaniowej), a następnie korzystając z kinematyki odwrotnej przeliczane są do reprezentacji w przestrzeni złączy. Nie jest to jedyne podejście, ale w tym ćwiczeniu ograniczymy się do takiego podejścia.

### 3. Obliczenie błędu położenia dla każdej z osi robota

Pierwsze zadanie programistyczne polega na obliczeniu błędu pozycji dla każdej z osi obliczanej jako najkrótsza odległość kątowa między aktualną konfiguracją robota a konfiguracją zadaną. Obliczenie to zachodzi w metodzie [`JointController::jointPositionError` (linia 188)](src/sipr_lab3/JointController.cpp#L188).

```c++
std::vector<double> JointController::jointPositionError(
        std::vector<double> current_joint_positions, 
        std::vector<double> target_joint_positions)
{
    std::vector<double> joint_position_errors(current_joint_positions.size());

    for(int i=0; i<int(current_joint_positions.size()); ++i)
    {
        double error = 0.0; // Tutaj, dla i-tego złącza, należy obliczyć
                            // różnicę między jego aktualną pozycją 
                            // a pozycją zadaną.
                            // Należy skorzystać z funkcji
                            // double angles::shortest_angular_distance(from, to)
                            // obliczającą najkrótszą odległość między dwoma kątami.
        joint_position_errors[i] = error;
    }

    return joint_position_errors;
}
```

Po modyfikacji tej funkcji, zbudowaniu przestrzeni roboczej i uruchomieniu węzła robot powinien zacząć się poruszać, choć pokonana ścieżka będzie daleka od najkrótszej.

![image](doc/figures/rviz_p3.png)

### 4. Sterowanie do pozycji zadanej w przestrzeni złączy - dobór nastaw regulatora PID

Prędkość dla każdej z osi jest wyznaczana na podstawie wyliczonego wcześniej błędu pozycji $e_i$. Najprostszym podejściem do sterowania jest zastosowanie regulatora PID, który oblicza sygnał sterujący w zależności od wartości błędu.

$$\theta_i(t) = K_p e_i(t) + K_i \int_0^t e_i(t) dt + K_d \frac{e_i(t)}{dt}$$

Tutaj ma to miejsce w metodzie `JointController::jointPositionControl`:

```c++
        joint_velocities[i] = joint_pid_controllers[i].calculate(error);
```

Sama implementacja regulatora znajduje się w klasie `Pid` zaimplementowanej w plikach [`include/sipr_lab3/src/Pid.h`](include/sipr_lab3/Pid.h) i  [`src/sipr_lab3/src/Pid.cpp`](src/sipr_lab3/Pid.cpp).

Co istotne dla każdej z osi tworzony jest osobny obiekt realizujący działanie regulatora PID. Ma to miejsce w metodzie `JointController::initPidControllers`.

```c++
void JointController::initPidControllers()
{
    int n = kinematic_model_->getVariableCount();

    // Należy dobrać odpowiednie wartości tak, aby robot
    // utrzymywał odpowiednią prędkość przy ruchu
    // liniowym i nie miał przeregulowania w punktach docelowych
    double max = 0.5;   // max prędkość obrotowa danej osi [rad/s]
    double min = -0.5;  // min prędkość obrotowa danej osi [rad/s]
    double Kp = 1.0; // wzmocnienie części proporcjonalnej regulatora PID
    double Kd = 0.1; // wzmocnienie części różniczkującej regulatora PID
    double Ki = 1.0; // wzmocnienie części całkującej regulatora PID

    for(int i = 0; i < n; i++)
    {
        Pid joint_pid = Pid( time_step, max, min, Kp, Kd, Ki );
        joint_pid_controllers.push_back(joint_pid);
    }
}
```

W tym punkcie ćwiczenia należy dobrać odpowiednie wartości współczynników wzmocnienia i ograniczeń prędkości tak, aby robot utrzymywała odpowiednią prędkość przy ruchu liniowym i nie miał znaczącego przeregulowania w punktach docelowych.

![image](doc/figures/rviz_p4.png)

### 5. Obliczenie punktów pośrednich dla ruchu po linii prostej (interpolacja liniowa ruchu w przestrzeni zadaniowej)

Ruch po linii prostej uzyskuje się w poprzez wyznaczenie pośrednich pozycji wzdłuż zadanej prostej, czyli interpolację.

Interpolacja liniowa między dwiema zadanymi pozycjami w przestrzeni kartezjańskiej jest realizowana w metodzie `JointController::getInterpolated3dPose`:

```c++
Eigen::Isometry3d JointController::getInterpolated3dPose(ros::Time time)
{
    ros::Duration d = (time - path_tracking_start_time);
    double p = d.toSec() / path_traversal_time;
    p = std::min(p, 1.0);

    Eigen::Isometry3d interpolated_pose;
    interpolated_pose.setIdentity();
    // Osobna interpolacja obrotu ...
    interpolated_pose.matrix().block<3,3>(0,0) = getInterpolatedRotation(p);
    // ... i przemieszczenia
    interpolated_pose.matrix().block<3,1>(0,3) = getInterpolatedPoint(p);

    return interpolated_pose;
}
```

W linii 336 dla danej chwili wyliczany jest postęp wzdłuż interpolowanej linii jako parametr `p` zmieniający wartości w przedziale `<0,1>`, gdzie `0` oznacza pozycję początkową, a `1` pozycję końcową.

Interpolacja liniowa między dwiema pozycjami obiektu w przestrzeni kartezjańskiej przeprowadzana jest osobno dla orientacji i przemieszczenia.

Interpolacja liniowa orientacji nie jest zadaniem oczywistym. Dobre efekty daje zastosowanie algorytmu SLERP działającego na kwaternionach, co zostało zaimplementowane w metodzie `JointController::getInterpolatedRotation`.

Interpolacja liniowa punktów na prostej jest już prostsza i stanowi kolejne zadanie programistyczne. Należy odpowiednio zmodyfikować linię 380 w metodzie `JointController::getInterpolatedPoint`.

```c++
Eigen::Vector3d JointController::getInterpolatedPoint(double p) 
{
    Eigen::Vector3d xyz_interpolated;
    
    Eigen::Vector3d xyz_1 =  tool_pose_1.translation();
    Eigen::Vector3d xyz_2 =  tool_pose_2.translation();

    // Poniższą linię należy zastąpić obliczeniem
    // punktu na prostej dla danego
    // parametru p, który zmienia wartości od 0 do 1, 
    // i reprezentuje postęp wzdłuż linii prostej.
    xyz_interpolated = xyz_2; // tutaj jakaś funkcja xyz_1, xyz_2 i p

    return xyz_interpolated;
}
```

Efekt interpolacji liniowej będzie podobny do poniższego:

![image](doc/figures/rviz_p5.png)

### 6. Testy i analiza otrzymanego rozwiązania

#### 6.1. Konfiguracja końcowa

Na tym etapie program robota powinien być realizowany w całości zgodnie z definicją w metodzie `JointController::defineRobotProgram`. Według definicji programu w punkcie `p3` robot powinien wrócić do pozycji początkowej, która odpowiada sytuacji w chwili uruchomienia symulacji, kiedy wszystkie osie są w pozycjach zerowych.

```c++
    RobotProgramInstruction p3;
    p3.interpolation = Interpolation::Joint; // Ustawienie interpolacji w przestrzeni węzłów
    p3.position = forwardKinematics(std::vector<double>(6, 0.0)); // Ustawienie pozycji zerowej
    robot_program_.push_back(p3); // Dodanie instrukcji do programu robota
```

Jednak konfiguracja końcowa robota, mimo poprawnego osiągnięcia pozycji narzędzia, nie jest identyczna z konfiguracją początkową. **Proszę spróbować wyjaśnić dlaczego tak się dzieje?**

![image](doc/figures/rviz_p0.png)

![image](doc/figures/rviz_p5.png)

#### 6.2. Testowanie programu dla losowych pozycji

Ostatnim zadaniem jest przetestowanie programu robota dla losowych pozycji `p1` i `p2`. W tym celu należy w definicji programu zakomentować nadanie konkretnych pozycji i odkomentować nadanie pozycji losowych:

```c++
    p1.position = getRandomPosition(); // Nadanie losowej pozycji, można zastąpić własnymi danymi wg wzoru poniżej
    // // Nadanie wartości macierzy przekształcenia jednorodnego:
    // p1.position.setIdentity();
    // p1.position.matrix().block<3,1>(0,3) = Eigen::Vector3d(1.2, -0.4, 0.6);
    // Eigen::Matrix3d p1_rotation(  Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ())
    //                             * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitY())
    //                             * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitX()));
    p1.position.matrix().block<3,3>(0,0) = p1_rotation;
```

Należy kilkakrotnie uruchomić węzeł `sipr_lab3_node` zwracając uwagę na to:

* czy robot zawsze był w stanie wykonać cały program?
* czy robot był zawsze w stanie wykonać poprawny przejazd wzdłuż linii prostej?
* czy człony robota wchodziły ze sobą w kolizję?

Proszę udzielić odpowiedniego komentarza w sprawozdaniu.
