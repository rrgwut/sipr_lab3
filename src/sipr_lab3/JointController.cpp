#include "sipr_lab3/JointController.h"

#include <eigen_conversions/eigen_msg.h>
#include <tf2_eigen/tf2_eigen.h>
#include <angles/angles.h>
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>


double calcEuclideanDistance(Eigen::Vector3d from, Eigen::Vector3d to)
{
    double distance = (to-from).norm();
    return distance;
}

double calcEuclideanDistance(Eigen::Isometry3d from, Eigen::Isometry3d to)
{
    return calcEuclideanDistance(from.translation(), to.translation());
}

double calcAngularDistance(Eigen::Isometry3d from, Eigen::Isometry3d to)
{
    Eigen::Vector3d from_rot_xyz = from.rotation().eulerAngles(0,1,2);
    Eigen::Vector3d to_rot_xyz = to.rotation().eulerAngles(0,1,2);
    
    Eigen::Vector3d error_rot;
    
    error_rot[0] = angles::shortest_angular_distance(from_rot_xyz[0], to_rot_xyz[0]); // error_rot_x
    error_rot[1] = angles::shortest_angular_distance(from_rot_xyz[1], to_rot_xyz[1]); // error_rot_y
    error_rot[2] = angles::shortest_angular_distance(from_rot_xyz[2], to_rot_xyz[2]); // error_rot_z

    return error_rot.norm();
}

Eigen::MatrixXd pseudoInverse(const Eigen::MatrixXd& u_matrix, const Eigen::MatrixXd& v_matrix, const Eigen::MatrixXd& s_diagonals)
{
  return v_matrix * s_diagonals.inverse() * u_matrix.transpose();
}

JointController::JointController() : nh_("~"), 
                                        path_tracking_velocity(0.1),    //[m/s]
                                        fixed_frame_id("base_link"), 
                                        time_step(1.0/10),              //[s]
                                        distance_tolerance(0.01),       //[m]
                                        angle_tolerance(0.05)           //[rad]
{
    robot_model_loader_ = robot_model_loader::RobotModelLoaderPtr(
            new robot_model_loader::RobotModelLoader("robot_description"));
    
    kinematic_model_ = robot_model_loader_->getModel();
    joint_model_group_ = kinematic_model_->getJointModelGroup("manipulator");
    
    ROS_INFO("Model frame: %s", kinematic_model_->getModelFrame().c_str());
    
    current_robot_state_ = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model_));
    current_robot_state_->setToDefaultValues();
    
    pub_joint_jog_ = nh_.advertise<control_msgs::JointJog>("joint_jog",1);
    sub_joint_states_ = nh_.subscribe("joint_states", 1, &JointController::jointStatesCallback, this);
    
    pub_current_target_ = nh_.advertise<geometry_msgs::PoseStamped>("/current_target", 1);
    pub_current_pose_ = nh_.advertise<geometry_msgs::PoseStamped>("/current_pose", 1);
    pub_programmed_positions_ = nh_.advertise<geometry_msgs::PoseArray>("/programmed_positions", 1, true);
    
    initPidControllers();
    defineRobotProgram();
    robot_program_step_ = 0;
    publishRobotProgramPositions();
}

JointController::~JointController() 
{
    
}

geometry_msgs::PoseStamped JointController::isometry3dToPoseStamped(Eigen::Isometry3d const& eigen_pose)
{
    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.frame_id = fixed_frame_id;
    pose_stamped.header.stamp = ros::Time::now();
    
    tf::poseEigenToMsg (eigen_pose, pose_stamped.pose);
    
    return pose_stamped;
}

Eigen::Isometry3d const& JointController::currentToolPosition()
{
    return current_robot_state_->getGlobalLinkTransform("tool0");
}

std::vector< double > JointController::currentJointPositions()
{
    std::vector< double > joint_values;
    current_robot_state_->copyJointGroupPositions(joint_model_group_, joint_values);
    return joint_values;
}

void JointController::defineRobotProgram()
{
    robot_program_.clear();

    RobotProgramInstruction p1;
    p1.interpolation = Interpolation::Joint; // Ustawienie interpolacji w przestrzeni węzłów
    // p1.position = getRandomPosition(); // Nadanie losowej pozycji, można zastąpić własnymi danymi wg wzoru poniżej
    // Nadanie wartości macierzy przekształcenia jednorodnego:
    p1.position.setIdentity();
    p1.position.matrix().block<3,1>(0,3) = Eigen::Vector3d(1.2, -0.4, 0.6);
    Eigen::Matrix3d p1_rotation(  Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitX()));
    p1.position.matrix().block<3,3>(0,0) = p1_rotation;
    robot_program_.push_back(p1); // Dodanie instrukcji do programu robota


    RobotProgramInstruction p2;
    p2.interpolation = Interpolation::Linear; // Ustawienie interpolacji w przestrzeni kartezjańskiej
    // p2.position = getRandomPosition(); // Nadanie losowej pozycji, można zastąpić własnymi danymi wg wzoru poniżej
    // Nadanie wartości macierzy przekształcenia jednorodnego:
    p2.position.setIdentity();
    p2.position.matrix().block<3,1>(0,3) = Eigen::Vector3d(1.2, 0.4, 0.6);
    Eigen::Matrix3d p2_rotation(  Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitZ())
                                * Eigen::AngleAxisd(0.0, Eigen::Vector3d::UnitY())
                                * Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitX()));
    p2.position.matrix().block<3,3>(0,0) = p2_rotation;
    robot_program_.push_back(p2); // Dodanie instrukcji do programu robota


    RobotProgramInstruction p3;
    p3.interpolation = Interpolation::Joint; // Ustawienie interpolacji w przestrzeni węzłów
    p3.position = forwardKinematics(std::vector<double>(6, 0.0)); // Ustawienie pozycji zerowej
    robot_program_.push_back(p3); // Dodanie instrukcji do programu robota
}

void JointController::publishRobotProgramPositions()
{
    geometry_msgs::PoseArray pose_array;
    pose_array.header.frame_id = fixed_frame_id;
    pose_array.header.stamp = ros::Time::now();
    
    for(RobotProgramInstruction p : robot_program_)
    {
        geometry_msgs::Pose pose;
        tf::poseEigenToMsg (p.position, pose);
        pose_array.poses.push_back(pose);
    }

    pub_programmed_positions_.publish(pose_array);
}

Eigen::Isometry3d JointController::getRandomPosition()
{   
    robot_state::RobotState robot_state(kinematic_model_); 
    robot_state.setToRandomPositions(joint_model_group_);
    return robot_state.getGlobalLinkTransform("tool0");
}

Eigen::Isometry3d JointController::forwardKinematics(std::vector< double > joint_positions)
{
    robot_state::RobotState robot_state(kinematic_model_);
    robot_state.setJointGroupPositions(joint_model_group_, joint_positions);
    return robot_state.getGlobalLinkTransform("tool0");    
}

std::vector<double> JointController::inverseKinematics(Eigen::Isometry3d end_effector_state)
{
    std::vector<double> joint_values;

    robot_state::RobotState robot_state(kinematic_model_);

    double timeout = 0.1;
    bool found_ik = robot_state.setFromIK(joint_model_group_, end_effector_state, timeout);

    if (found_ik && robot_state.satisfiesBounds())
    {
        robot_state.copyJointGroupPositions(joint_model_group_, joint_values);
    }
    else
    {
        ROS_ERROR("Did not find IK solution");
        ros::shutdown();
    }

    return joint_values; //Uwaga:joint_values będzie puste jeżeli nie uda się 
                         //znaleźć rozwiązania kinematyki odwrotnej
}

std::vector<double> JointController::jointPositionError(
        std::vector<double> current_joint_positions, 
        std::vector<double> target_joint_positions)
{
    std::vector<double> joint_position_errors(current_joint_positions.size());

    for(int i=0; i<int(current_joint_positions.size()); ++i)
    {
        double error = 0.0; // Tutaj, dla i-tego złącza, należy obliczyć
                            // różnicę między jego aktualną pozycją 
                            // a pozycją zadaną.
                            // Należy skorzystać z funkcji
                            // double angles::shortest_angular_distance(from, to)
                            // obliczającą najkrótszą odległość między dwoma kątami.
        joint_position_errors[i] = error;
    }

    return joint_position_errors;
}

std::vector<double> JointController::jointPositionErrorInverseJacobian(
        Eigen::Isometry3d current_position, 
        Eigen::Isometry3d target_position)
{   
    // Pobranie jakobianu dla aktualnej pozycji robota
    Eigen::MatrixXd jacobian = current_robot_state_->getJacobian(joint_model_group_);
    // Obliczanie jakobianu odwrotnego za pomocą pseudoinwersji macierzy
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(jacobian, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::MatrixXd inverse_jacobian = pseudoInverse(svd.matrixU(), svd.matrixV(), svd.singularValues().asDiagonal());

    // Obliczenie błędu pozycji w przestrzeni kartezjańskiej
    Eigen::Vector3d error_xyz = target_position.translation() - current_position.translation();
    Eigen::Vector3d current_rot_xyz = current_position.rotation().eulerAngles(0,1,2);
    Eigen::Vector3d target_rot_xyz = target_position.rotation().eulerAngles(0,1,2);
    
    Eigen::VectorXd error_3D(6);
    error_3D << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    //wpisać odpowiednie wartości
    // error_3D[0] = ??; // error_x
    // error_3D[1] = ??; // error_y
    // error_3D[2] = ??; // error_z
    // error_3D[3] = ??; // error_rot_x
    // error_3D[4] = ??; // error_rot_y
    // error_3D[5] = ??; // error_rot_z

    Eigen::VectorXd error_joints(6);
    error_joints << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0; 
    // powyższą linię zakomentować i zastąpić ją obliczeniem error_joints korzystając
    // z mnożenia jakobianu odwrotnego i błędu w przestrzeni kartezjańskiej (error_3D):
    //error_joints = ??; 

    int n = kinematic_model_->getVariableCount();
    std::vector<double> joint_position_errors(n);

    for(int i=0; i<n; ++i)
    {
        joint_position_errors[i] = error_joints[i];
    }

    return joint_position_errors;
}

void JointController::initPidControllers()
{
    int n = kinematic_model_->getVariableCount();
    
    // Należy dobrać odpowiednie wartości tak, aby robot
    // utrzymywał odpowiednią prędkość przy ruchu
    // liniowym i nie miał przeregulowania w punktach docelowych
    double max = 0.5;   // max prędkość obrotowa danej osi [rad/s]
    double min = -0.5;  // min prędkość obrotowa danej osi [rad/s]
    double Kp = 1.0; // wzmocnienie części proporcjonalnej regulatora PID
    double Kd = 0.1; // wzmocnienie części różniczkującej regulatora PID
    double Ki = 1.0; // wzmocnienie części całkującej regulatora PID

    for(int i = 0; i < n; i++)
    {
        Pid joint_pid = Pid( time_step, max, min, Kp, Kd, Ki );
        joint_pid_controllers.push_back(joint_pid);
    }
}

std::vector<double> JointController::jointPositionControl(
        std::vector<double> joint_position_errors)
{
    std::vector<double> joint_velocities(joint_position_errors.size());

    for(int i=0; i<int(joint_position_errors.size()); ++i)
    {
        double error = joint_position_errors[i];
        // Wyliczenie prędkości i-tej osi dla danego błędu pozycji kątowej
        joint_velocities[i] = joint_pid_controllers[i].calculate(error);
    }

    return joint_velocities;
}

void JointController::publishJointJog(std::vector<double> joint_velocities)
{
    int n = kinematic_model_->getVariableCount();
    
    control_msgs::JointJog joint_jog_msg;
    joint_jog_msg.header.stamp = ros::Time::now();
    joint_jog_msg.joint_names = kinematic_model_->getVariableNames();
    joint_jog_msg.velocities.resize(n,0.0);
    joint_jog_msg.velocities = joint_velocities;
    pub_joint_jog_.publish(joint_jog_msg);
}

void JointController::publishJointJogStop()
{
    int n = kinematic_model_->getVariableCount();
    std::vector<double> zero_joint_velocities(n, 0.0);
    publishJointJog(zero_joint_velocities);
}

bool JointController::positionReached(Eigen::Isometry3d current_pose, Eigen::Isometry3d target_pose)
{
    double euclidean_distance = calcEuclideanDistance(current_pose, target_pose);  
    double angular_distance = calcAngularDistance(current_pose, target_pose);  
    return (euclidean_distance <= distance_tolerance) && (angular_distance <= angle_tolerance);
}

bool JointController::initializeLinearMotion(Eigen::Isometry3d current_pose, Eigen::Isometry3d target_pose)
{
    tool_pose_1 = current_pose;
    tool_pose_2 = target_pose;
    path_tracking_start_time = ros::Time::now();
    computePathTraversalTime();
    linear_motion_initialized = true;
}
    
bool JointController::finishLinearMotion()
{
    linear_motion_initialized = false;
}

void JointController::computePathTraversalTime()
{
    double distance = calcEuclideanDistance(tool_pose_1, tool_pose_2);
    path_traversal_time = distance / path_tracking_velocity;
    
    ROS_INFO("Estimated path traversal time: %f", path_traversal_time);
}

Eigen::Isometry3d JointController::getInterpolated3dPose(ros::Time time)
{
    ros::Duration d = (time - path_tracking_start_time);
    double p = d.toSec() / path_traversal_time;
    p = std::min(p, 1.0);
        
    Eigen::Isometry3d interpolated_pose;
    interpolated_pose.setIdentity();
    // Osobna interpolacja obrotu ...
    interpolated_pose.matrix().block<3,3>(0,0) = getInterpolatedRotation(p);
    // ... i przemieszczenia
    interpolated_pose.matrix().block<3,1>(0,3) = getInterpolatedPoint(p);
        
    return interpolated_pose;    
}

Eigen::Matrix3d JointController::getInterpolatedRotation(double p)
{
    Eigen::Quaterniond eq1(tool_pose_1.rotation());
    Eigen::Quaterniond eq2(tool_pose_2.rotation());
    tf::Quaternion q1;
    tf::Quaternion q2;

    tf::quaternionEigenToTF(eq1,q1);
    tf::quaternionEigenToTF(eq2,q2);

    // Interpolacja obrotu jest trudniejsza niż interpolacja przemieszczenia.
    // W tym celu stosuje się algorytm SLERP działający na kwaternionach
    tf::Quaternion q_interpolated = q1.slerp(q2, p);

    Eigen::Quaterniond eq_interpolated;
    tf::quaternionTFToEigen(q_interpolated,eq_interpolated);
    
    return Eigen::Matrix3d(eq_interpolated);
}

Eigen::Vector3d JointController::getInterpolatedPoint(double p) 
{
    Eigen::Vector3d xyz_interpolated;
    
    Eigen::Vector3d xyz_1 =  tool_pose_1.translation();
    Eigen::Vector3d xyz_2 =  tool_pose_2.translation();

    // Poniższą linię należy zastąpić obliczeniem
    // punktu na prostej dla danego
    // parametru p, który zmienia wartości od 0 do 1, 
    // i reprezentuje postęp wzdłuż linii prostej.
    xyz_interpolated = xyz_2; // tutaj jakaś funkcja xyz_1, xyz_2 i p

    return xyz_interpolated;
}

void JointController::jointStatesCallback(sensor_msgs::JointState const& joint_states_msg)
{
    // Aktualizacja stanu robota
    current_robot_state_->setVariableValues(joint_states_msg);

    // Publikowanie aktualnej pozycji narzędzia (żółta strzałka w rviz)
    const Eigen::Isometry3d& current_tool_position = currentToolPosition();
    pub_current_pose_.publish(isometry3dToPoseStamped(current_tool_position));

    if(robot_program_step_ >= int(robot_program_.size()))
    {
        ROS_INFO("Robot program finished");
        ros::shutdown();
        return;
    }

    RobotProgramInstruction instruction = robot_program_[robot_program_step_];

    if(positionReached(current_tool_position, instruction.position))
    { 
        ROS_INFO("Program step %d - %s motion - finished.", 
            (robot_program_step_+1), 
            ((instruction.interpolation==Interpolation::Joint)?"joint":"linear"));
        publishJointJogStop(); 

        if(instruction.interpolation == Interpolation::Linear)
        {
            finishLinearMotion();
        }

        robot_program_step_++;
    }
    else
    {
        Eigen::Isometry3d target;

        if(instruction.interpolation == Interpolation::Joint)
        {
            target = instruction.position;
        }
        else if(instruction.interpolation == Interpolation::Linear)
        {
            if(!linear_motion_initialized)
            {
                initializeLinearMotion(current_tool_position, instruction.position);
            }

            target = getInterpolated3dPose(ros::Time::now());
        }

        pub_current_target_.publish(isometry3dToPoseStamped(target));

        ///////////// Obliczenie kierunków przemieszczania konkretnych złączy
        std::vector<double> joint_position_errors;
        
        ///////////// Metoda 1
        std::vector<double> target_joint_values = inverseKinematics(target);
        if(!target_joint_values.empty())
        {
            joint_position_errors = jointPositionError(
                currentJointPositions(), target_joint_values);
        }

        ///////////// Metoda 2 - należy sprawdzić jej działanie w zamian metody 1
        // joint_position_errors = jointPositionErrorInverseJacobian(
        //     current_tool_position, target);
        
        std::vector<double> joint_velocities = jointPositionControl(joint_position_errors);
        publishJointJog(joint_velocities);
    }
}
