#include <cstdlib>
#include <sipr_lab3/JointController.h>
#include <sipr_lab3/Simulator.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    ros::init(argc, argv, "sipr_lab3_node");
    
    JointController controller;    
    Simulator simulator;
    
    ros::Rate loop_rate(10);
    
    while(ros::ok())
    {
        ros::spinOnce();        
        simulator.update();        
        loop_rate.sleep();
    }

    return 0;
}
