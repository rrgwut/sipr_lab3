#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <control_msgs/JointJog.h>
#include <sensor_msgs/JointState.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

class Simulator 
{
public:
    explicit Simulator();

    virtual ~Simulator();
    
    void update();
private:
    ros::NodeHandle nh_;
    ros::Subscriber sub_joint_jog_;
    ros::Publisher pub_joint_states_;
    ros::Publisher pub_traveled_path_;
    
    control_msgs::JointJog joint_jog_msg_;
    geometry_msgs::PoseArray traveled_path_;
    
    robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
    robot_model::RobotModelPtr kinematic_model_;
    robot_state::RobotStatePtr robot_state_;
    robot_state::JointModelGroup const* joint_model_group_ = nullptr;
    
    ros::Time prev_time_;
    bool time_set_ = false;

    void jointJogCallback(control_msgs::JointJog const& joint_jog_msg);
};

#endif /* SIMULATOR_H */

