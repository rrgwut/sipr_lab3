#ifndef PID_H
#define PID_H

class Pid
{
    public:
        Pid( double dt, double max, double min, double Kp, double Kd, double Ki );
        ~Pid();
        double calculate( double error );

    private:
        double _dt;
        double _max;
        double _min;
        double _Kp;
        double _Kd;
        double _Ki;
        double _pre_error;
        double _integral;
};

#endif /* PID_H */

