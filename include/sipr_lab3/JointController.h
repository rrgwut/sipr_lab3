#ifndef JOINTCONTROLLER_H
#define JOINTCONTROLLER_H

#include "sipr_lab3/Pid.h"

#include <ros/ros.h>
#include <control_msgs/JointJog.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/Path.h>
#include <control_toolbox/pid.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

enum class Interpolation
{
    Joint,
    Linear
};

struct RobotProgramInstruction
{
    Interpolation interpolation;
    Eigen::Isometry3d position;
};

class JointController 
{
public:
    explicit JointController();

    virtual ~JointController();
    
private:
    ros::NodeHandle nh_;
    ros::Subscriber sub_joint_states_;
    ros::Publisher pub_joint_jog_;
    ros::Publisher pub_programmed_positions_;  
    ros::Publisher pub_current_target_;
    ros::Publisher pub_current_pose_;
        
    robot_model_loader::RobotModelLoaderPtr robot_model_loader_;
    robot_model::RobotModelPtr kinematic_model_;
    robot_state::RobotStatePtr current_robot_state_;
    robot_state::JointModelGroup const* joint_model_group_ = nullptr;
    
    std::string fixed_frame_id;
    double time_step;
    std::vector< Pid > joint_pid_controllers;
    std::vector<RobotProgramInstruction> robot_program_;
    int robot_program_step_;
    
    double distance_tolerance;
    double angle_tolerance;
        
    double path_tracking_velocity;
    Eigen::Isometry3d tool_pose_1, tool_pose_2;
    bool linear_motion_initialized = false;
    double path_traversal_time;
    ros::Time path_tracking_start_time;

    geometry_msgs::PoseStamped isometry3dToPoseStamped(Eigen::Isometry3d const& eigen_pose);
    Eigen::Isometry3d const& currentToolPosition();
    std::vector< double > currentJointPositions();
    Eigen::Isometry3d getRandomPosition();
    Eigen::Isometry3d forwardKinematics(std::vector< double > joint_positions);
    std::vector< double > inverseKinematics(Eigen::Isometry3d end_effector_state);

    void defineRobotProgram();
    void publishRobotProgramPositions();

    std::vector<double> jointPositionError(
        std::vector<double> current_joint_positions, 
        std::vector<double> target_joint_positions);

    std::vector<double> jointPositionErrorInverseJacobian(
        Eigen::Isometry3d current_position, 
        Eigen::Isometry3d target_position);

    void initPidControllers();

    std::vector<double> jointPositionControl(
        std::vector<double> joint_position_errors);
    
    void publishJointJog(std::vector<double> joint_velocities);
    void publishJointJogStop();
    
    bool positionReached(Eigen::Isometry3d current_pose, Eigen::Isometry3d target_pose);

    void computePathTraversalTime();
    bool initializeLinearMotion(Eigen::Isometry3d current_pose, Eigen::Isometry3d target_pose);
    bool finishLinearMotion();    
    Eigen::Vector3d getInterpolatedPoint(double t);
    Eigen::Matrix3d getInterpolatedRotation(double t);
    Eigen::Isometry3d getInterpolated3dPose(ros::Time time);

    void jointStatesCallback(sensor_msgs::JointState const& joint_states_msg);
};

#endif /* JOINTCONTROLLER_H */

